import { StatusBar } from 'expo-status-bar'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import * as Facebook from 'expo-facebook'
import { SafeAreaProvider } from 'react-native-safe-area-context'

const App = () => {
  const [ message, setMessage ] = useState<string>('initialization...')
  
  const initialize = async () => {
    Facebook.initializeAsync({
      appId: '449272369657338',
      appName: 'Facebook SDK'
    })
      .then(async () => {
        setMessage('App has been initialized successfully')
        await Facebook.setAdvertiserIDCollectionEnabledAsync(true)
        await Facebook.setAdvertiserTrackingEnabledAsync(true)
        await Facebook.setAutoLogAppEventsEnabledAsync(true)
        await Facebook.flushAsync()
        await Facebook.logEventAsync('ANROM event', { page: 'home' })
      })
      .catch((err: any) => {
        setMessage(`Error: ${ err }`)
        console.error('Error', err)
      })
  }

  useEffect(() => { initialize() }, [])

  return (
    <SafeAreaProvider>
      <View style={styles.container}>
        <Text>
          { message }
        </Text>

        <StatusBar style="auto" />
      </View>
    </SafeAreaProvider>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export default App